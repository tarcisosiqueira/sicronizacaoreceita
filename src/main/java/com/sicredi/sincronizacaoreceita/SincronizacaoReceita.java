package com.sicredi.sincronizacaoreceita;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/*
CenÃ¡rio de NegÃ³cio:
Todo dia Ãºtil por volta das 6 horas da manhÃ£ um colaborador da retaguarda do Sicredi recebe e organiza as informaÃ§Ãµes de contas para enviar ao Banco Central. Todas agencias e cooperativas enviam arquivos Excel Ã  Retaguarda. Hoje o Sicredi jÃ¡ possiu mais de 4 milhÃµes de contas ativas.
Esse usuÃ¡rio da retaguarda exporta manualmente os dados em um arquivo CSV para ser enviada para a Receita Federal, antes as 10:00 da manhÃ£ na abertura das agÃªncias.

Requisito:
Usar o "serviÃ§o da receita" (fake) para processamento automÃ¡tico do arquivo.

Funcionalidade:
0. Criar uma aplicaÃ§Ã£o SprintBoot standalone. Exemplo: java -jar SincronizacaoReceita <input-file>
1. Processa um arquivo CSV de entrada com o formato abaixo.
2. Envia a atualizaÃ§Ã£o para a Receita atravÃ©s do serviÃ§o (SIMULADO pela classe ReceitaService).
3. Retorna um arquivo com o resultado do envio da atualizaÃ§Ã£o da Receita. 
	Mesmo formato adicionando o resultado em uma nova coluna.


Formato CSV:
agencia;conta;saldo;status
0101;12225-6;100,00;A
0101;12226-8;3200,50;A
3202;40011-1;-35,12;I
3202;54001-2;0,00;P
3202;00321-2;34500,00;B
...

*/

@RestController
@Api(value = "")
public class SincronizacaoReceita {

	@ApiOperation(value = "")
	@GetMapping(value = "/cnab")
	public static void main(String[] args) throws RuntimeException, InterruptedException, IOException {

		// Exemplo como chamar o "serviÃ§o" do Banco Central.
		// ReceitaService receitaService = new ReceitaService();
		// receitaService.atualizarConta("0101", "123456", 100.50, "A");
		
		ReceitaService receitaService = new ReceitaService();

		// Aqui o caminho e nome do arquivo estão setado hardcode
		// uma melhoria para o futuro seria implantar o recebimento do arquivo
		// via upload, criando um simples endpoint
		final String HOMEPATH = "C:\\Users\\VISITAS\\data\\";
		final Reader reader = Files.newBufferedReader(Paths.get(HOMEPATH+"sicredi\\CNAB.CSV"));

		final CSVParser parser = new CSVParserBuilder().withSeparator(';').withIgnoreQuotations(true).build();
		final CSVReader csvReader = new CSVReaderBuilder(reader).withSkipLines(1).withCSVParser(parser).build();

		List<String[]> linhas = csvReader.readAll();
		FileWriter arq = new FileWriter(HOMEPATH+"sicredi\\RET-CNAB.CSV");
		PrintWriter gravarArq = new PrintWriter(arq);
		String status = new String();
		
		for (String[] coluna : linhas) {
			System.out.println("agencia: " + coluna[0] + " - conta: " + coluna[1] + " - saldo: " + coluna[2]
					+ " - status: " + coluna[3]);
			
			try {
				receitaService.atualizarConta(coluna[0]
											 ,coluna[1].replaceAll("-", "")
											 ,Double.parseDouble(coluna[2].replaceAll(",", "."))
											 ,coluna[3]);
				status = "Enviado";
			} catch (Exception e) {
				status = "Não Enviado";
			}
			gravarArq.printf(coluna[0] + ";" + coluna[1] + ";" + coluna[2] + ";" + coluna[3] + ";" + status + "\n");
		}
		arq.close();

	}

}
